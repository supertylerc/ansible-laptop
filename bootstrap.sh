#!/usr/bin/env bash

PACKAGES=()

install_ansible() {
  if ! lsvirtualenv | grep -q ansible-laptop; then
    echo "Couldn't find ansible-laptop venv.  Creating it and installing Ansible."
    mkvirtualenv -p python3 ansible-laptop
    workon ansible-laptop
    pip install -U setuptools
    pip install -U ansible jmespath
    fix_python_apt
  fi
}

check_git() {
  if ! type git > /dev/null; then
    echo "Couldn't find git.  Installing with apt-get."
    PACKAGES+=(git)
  fi
}

check_software_properties_common() {
  if ! dpkg -l software-properties-common | grep -q ^ii; then
    echo "Couldn't find software-properties-common.  Installing with apt-get."
    PACKAGES+=(software-properties-common)
  fi
}

check_python_distutils() {
  if ! dpkg -l python3-distutils | grep -q ^ii; then
    echo "Couldn't find python3-distutils.  Installing with apt-get."
    PACKAGES+=(python3-distutils)
  fi
}

check_aptitude() {
  if ! type aptitude; then
    echo "Couldn't find aptitude.  Installing with apt-get."
    PACKAGES+=(aptitude)
  fi
}

check_python() {
  if ! type python3 > /dev/null; then
    echo "Couldn't find python3.  Installing it with apt-get."
    PACKAGES+=(python3)
  fi
}
 
check_pip() {
  if ! type pip > /dev/null; then
    echo "Couldn't find pip3.  Installing it."
    curl -L https://bootstrap.pypa.io/get-pip.py | sudo python3
  fi
}

install_packages() {
  check_git
  check_software_properties_common
  check_python
  check_python_distutils
  if [[ "${#PACKAGES[@]}" -ne 0 ]]; then
    sudo apt-get -y update
    sudo apt-get -y install "${PACKAGES[@]}"
  fi
}

get_input() {
  echo -n "What is your Ansible repository? "
  read ANSIBLE_REPO
  if [[ ! -f "${HOME}/.gitconfig" ]]; then
    echo -n "What is your git name? "
    read GIT_USER
    echo -n "What is your git e-mail? "
    read GIT_EMAIL
  fi
}

setup_git() {
  if [[ ! -f "${HOME}/.gitconfig" ]]; then
    $GIT config --global user.name "${GIT_USER}"
    $GIT config --global user.email "${GIT_EMAIL}"
    $GIT config --global user.editor vim
  fi
}

setup_ansible() {
  install_ansible
  workon ansible-laptop
  if [[ ! -f "${HOME}/.ansible.cfg" ]]; then
    echo "Couldn't find ${HOME}/.ansible.cfg.  Creating it now."
    echo "[defaults]" > $HOME/.ansible.cfg
    echo "remote_tmp = /tmp/$USER/ansible" >> $HOME/.ansible.cfg
    echo "stdout_callback = yaml" >> $HOME/.ansible.cfg
    echo "bin_ansible_callbacks = True" >> $HOME/.ansible.cfg
    echo "inventory = ~/.ansible_inventory" >> $HOME/.ansible.cfg
  fi
  if [[ ! -f "${HOME}/.ansible_inventory" ]]; then
    echo "Couldn't find ${HOME}/.ansible_inventory.  Creating it now."
    echo localhost > $HOME/.ansible_inventory
  fi
}

fix_python_apt() {
  pushd /tmp
  apt-get download python3-apt
  dpkg -x python3-apt_*_amd64.deb python3-apt
  cp -r /tmp/python3-apt/usr/lib/python3/dist-packages/* $HOME/.virtualenvs/ansible-laptop/lib/python3.6/site-packages/
  popd
  pushd $HOME/.virtualenvs/ansible-laptop/lib/python3.6/site-packages/
  mv apt_inst.*so apt_inst.so
  mv apt_pkg.*so apt_pkg.so
  popd
}

setup_python() {
  PYTHON_VERSION=$(python --version)
  if [[ "${PYTHON_VERSION}" == "Python 2"* ]]; then
    echo "Setting default Python to Python3."
    sudo update-alternatives --install /usr/bin/python python /usr/bin/python3 10
    sudo update-alternatives --install /usr/bin/pip pip /usr/local/bin/pip 10
  fi
  if ! pip freeze --user | grep -q virtualenvwrapper; then
    echo "Couldn't find virtualenvwrapper, installing it."
    pip install --user virtualenvwrapper virtualenv
    case $SHELL in
      *bash) SHELL_FILE=$HOME/.bashrc ;;
      *zsh) SHELL_FILE=$HOME/.zshrc ;;
    esac
    echo 'export WORKON_HOME=$HOME/.virtualenvs' >> $SHELL_FILE
    echo 'export PROJECT_HOME=$HOME/Projects' >> $SHELL_FILE
    echo 'source $HOME/.local/bin/virtualenvwrapper.sh' >> $SHELL_FILE
    if ! egrep -q 'export PATH.*\.local/bin' $SHELL_FILE; then
      echo 'export PATH=$PATH:$HOME/.local/bin' >> $SHELL_FILE
      export PATH=$PATH:$HOME/.local/bin
    fi
  fi
  export WORKON_HOME=$HOME/.virtualenvs
  export PROJECT_HOME=$HOME/Projects
  source $HOME/.local/bin/virtualenvwrapper.sh
}

setup_sudo() {
  echo "Warning: this is probably brittle and may not work."
  echo "Setting /etc/sudoers.d/ansible"
  echo "${USER} ALL=(ALL) NOPASSWD: /home/${USER}/.virtualenvs/ansible-laptop/bin/ansible" | sudo tee --append /etc/sudoers.d/ansible > /dev/null
  echo "${USER} ALL=(ALL) NOPASSWD: /home/${USER}/.virtualenvs/ansible-laptop/bin/ansible-playbook" | sudo tee --append /etc/sudoers.d/ansible > /dev/null
  echo "${USER} ALL=(ALL) NOPASSWD: /home/${USER}/.virtualenvs/ansible-laptop/bin/ansible-pull" | sudo tee --append /etc/sudoers.d/ansible > /dev/null
}

main() {
  install_packages
  if [[ ! -z "${ANSIBLE_LAPTOP_SUDO}"  ]]; then
    setup_sudo
  fi
  GIT=$(which git)
  ANSIBLE_PULL=$(which ansible-pull)
  if [[ -z "${ANSIBLE_REPO}" ]]; then
    get_input
  fi
  setup_git
  check_pip
  setup_python
  setup_ansible
  if [[ ! -z "${ANSIBLE_LAPTOP_PULL}" ]]; then
    $ANSIBLE_PULL --accept-host-key --purge --verify-commit --only-if-changed --vault-password-file="${ANSIBLE_LAPTOP_VAULT}" --extra-vars "@${ANSIBLE_LAPTOP_SECRETS}" -U $ANSIBLE_REPO playbook.yml
  fi
}


main
