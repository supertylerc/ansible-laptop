#!/usr/bin/env python

# Copyright: (c) 2019, Tyler Christiansen <code@tylerc.me>
# GNU General Public License v3.0+ (see https://www.gnu.org/licenses/gpl-3.0.txt)

ANSIBLE_METADATA = {
    'metadata_version': '1.1',
    'status': ['preview'],
    'supported_by': 'community'
}

DOCUMENTATION = '''
---
module: vscode_install_extension

short_description: Module for installing vscode extensions.

version_added: "2.7"

description:
  - "Installs (but does not update or remove) vscode packages."

options:
  name:
    description:
      - Name of the package(s) to be installed.
    required: true
author:
    - Tyler Christiansen (@supertylerc)
'''

EXAMPLES = '''
# Install the Python vscode Extension
- name: Install Python vscode extension
  vscode_install_extension:
    name: ms-python.python
'''


from ansible.module_utils.basic import AnsibleModule


def is_installed(module, name):
    rc, out, err = module.run_command(['code', '--list-extensions', name])
    if rc != 0 or (err and '[DEP0005]' not in err):
        module.fail_json(
            msg='Error querying installed extensions [%s]: %s' % (name,
                                                                  out + err))
    lowername = name.lower()
    match = next((x for x in out.splitlines() if x.lower() == lowername), None)
    return match is not None


def install(module, name):
    rc, out, err = module.run_command(
        ['code', '--install-extension', name])
    if rc != 0 or (err and '[DEP0005]' not in err):
        module.fail_json(
            msg='Error while installing extension [%s]: (%d) %s' % (name,
                                                                    rc,
                                                                    out + err))


def run_module():
    module_args = dict(
        name=dict(type='list', required=True),
    )

    result = dict(
        changed=False,
        new=list()
    )

    module = AnsibleModule(
        argument_spec=module_args,
        supports_check_mode=False
    )
    for name in module.params["name"]:
        if not is_installed(module, name):
            install(module, name)
            result['new'].append(name)
            result['changed'] = True


    module.exit_json(**result)


def main():
    run_module()


if __name__ == '__main__':
    main()
