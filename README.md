# ansible-laptop

Manage an Ubuntu laptop with `ansible-pull`.

## Caveat Emptor

This is _extremely_ opinionated.  It typically prefers snaps over debs,
and it is opinionated to the point of switching the system default Python
from Python2 (which is EOL 1 January 2020) to Python3.  If you don't agree
with the default Python change, you can probably use this repo with your
own custom `bootstrap.sh`, as that is where it occurs.

I also haven't actually tested _any_ of this on a fresh system, so who knows
if it works.

## Design Principles

- Don't store any sensitive information in the repo.  Ever.
- Tag roles but not tasks inside of roles
- Use roles for everything
- Python2 is dead; long live Python3!
- `ansible-pull` in a cron job is better than having to remember to run `ansible-playbook`
- only use `become` where absolutely necessary, but do it at a role level instead of task
  level for simplicity
- corollary to the previous, never run as root by default
- only run if the commit has been signed
- roles should use role dependencies to manage their own dependences (like packages to install)
- when something doesn't deserve its own role (such as just installing htop), it should be
  handled with a generic role and the associated `host_vars` (like installing `htop`.  It
  needs no configuration, so we add it to the `packages__snap` `host_var` and let the `packages`
  role install it)

## Usage

> This applies only to the future.  None of this currently works.  For now, manually run
> `ansible-playbook --ask-become-pass -c local playbook.yml --diff` from this repo root.

1: Create an `ansible-vault` password: `date +%s | sha256sum | base64 | head -c 32 > ~/.vault`

2: Create an `ansible-vault` encrypted file and add the `ansible_become_pass` variable with your
   `sudo` password as the value: `ansible-vault --vault-password-file=~/.vault create ~/.secrets.yml`
   Example contents:

```yaml
ansible_become_pass: "this is a really cool and very secure password please don't steal it"
```

3: get and run `bootstrap.sh`

4: install `bootstrap.sh` as a cron job under _your user_, *not* root!

5: use `ansible-vault` plus a file with your `ansible-vault` secret to handle privilege
   escalation requirements; this is specified as env variable, `ANSIBLE_LAPTOP_SECRETS`

> If you take the `ansible-vault` route, do _not_ place any of the files in this repo!

Example for your cron job:

```bash
*/30 * * * * ANSIBLE_LAPTOP_SECRETS=/home/tyler/.secrets.yml ANSIBLE_LAPTOP_VAULT=/home/tyler/.vault ANSIBLE_LAPTOP_PULL=1 /home/tyler/.bootstrap.sh
```

> The above is currently untested.

If you're signing your `git` commits, this project doesn't set that up.  Set that up
manually with something like the following:

```bash
git config --global user.signingkey $YOUR_GPG_ID
git config --global commit.gpgsign true
```


## Improvements

- make the default Python interpreter switch configurable with a `bootstrap.sh` flag
- make the `host_vars/localhost.yml` source configurable so that this repo can be used
  as the source of all playbooks but variables can come from another personal repo.
  This would prevent the need for others to fork this repo to customize.
- improve the resiliency of the `vscode_install_extension` module
- when the `snap` module is released with upstream Ansible, remove the static version
  from this repo
- write tests :(
- add more things!
- make the bootstrap script install itself into a cronjob

## Thanks

The `snap` module is pulled directly from the current (as of this commit) `snap` module
in upstream Ansible.

The `vscode_install_extension` plugin is based on [this repo](https://github.com/gantsign/ansible-role-visual-studio-code).
